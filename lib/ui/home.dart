import 'package:flutter/material.dart';
import 'make_it_rain.dart';

class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Make It Rain!"),
        backgroundColor: Colors.lightGreen,
      ),
      body: MakeItRain(),
    );
  }

}