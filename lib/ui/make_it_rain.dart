import 'package:flutter/material.dart';

class MakeItRain extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return MakeItRainState();
  }

}

class MakeItRainState extends State<MakeItRain> {

  int _moneyCounter = 0;
  int _moneyThreshold = 10000;
  bool _thresholdReached = false;

  void _rainMoney() {
    setState(() {
      _moneyCounter += 100;
    });
    if (!_thresholdReached) {
      if (_moneyCounter >= _moneyThreshold) {
        setState(() {
          _thresholdReached = true;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Center(
            child: Text(
              "Get Rich!",
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.w400,
                  fontSize: 29.9
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: Text(
                "\$$_moneyCounter",
                style: _thresholdReached ?
                  TextStyle(
                    color: Colors.greenAccent,
                    fontSize: 58.3,
                    fontWeight: FontWeight.bold
                  ) :
                  TextStyle(
                    color: Colors.greenAccent,
                    fontSize: 46.9,
                    fontWeight: FontWeight.w800
                  ),
              ),
            )
          ),
          Expanded(
            child: Center(
              child: FlatButton(
                color: Colors.lightGreen,
                textColor: Colors.white70,
                onPressed: _rainMoney,
                child: Text(
                  "Let It Rain!",
                  style: TextStyle(
                    fontSize: 19.9,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}